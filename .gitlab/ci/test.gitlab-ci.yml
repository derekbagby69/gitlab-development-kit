docs-lint:
  extends: .rules:docs-changes
  image: registry.gitlab.com/gitlab-org/gitlab-docs/lint-markdown:alpine-3.13-vale-2.10.2-markdownlint-0.26.0
  stage: test
  needs: []
  script:
    - make lint

rubocop:
  extends: .rules:code-changes
  stage: test
  needs: []
  variables:
    BUNDLE_ARGS: "--path vendor/bundle"
  script:
    - make rubocop

rspec:
  extends: .rules:code-changes
  stage: test
  needs: []
  variables:
    RSPEC_ARGS: "--format doc --format RspecJunitFormatter --out rspec.xml"
    BUNDLE_ARGS: "--path vendor/bundle"
  script:
    - pwd > .gdk-install-root
    - make rspec
  cache:
    key: "ruby-2.7-bundle"
    paths:
      - ./vendor/bundle
  artifacts:
    paths:
      - rspec.xml
    reports:
      junit: rspec.xml
      cobertura: coverage/coverage.xml

include:
  - template: Code-Quality.gitlab-ci.yml

code_quality:
  extends:
    - .rules:code-changes
    - .docker:use-docker-in-docker
  needs: []
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For `default branch` branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'

container-scanning:
  extends:
    - .rules:code-changes
    - .docker:use-docker-in-docker
  stage: test
  allow_failure: true
  needs:
    - release-image
  script:
    - docker run -d --name db arminc/clair-db:latest
    - docker run -p 6060:6060 --link db:postgres -d --name clair --restart on-failure arminc/clair-local-scan:v2.0.1
    - apk add -U wget ca-certificates
    - docker pull ${VERIFY_IMAGE}
    - wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64
    - mv clair-scanner_linux_amd64 clair-scanner
    - chmod +x clair-scanner
    - touch clair-whitelist.yml
    - retries=0
    - echo "Waiting for clair daemon to start"
    - while( ! wget -T 10 -q -O /dev/null http://docker:6060/v1/namespaces ) ; do sleep 1 ; echo -n "." ; if [ $retries -eq 10 ] ; then echo " Timeout, aborting." ; exit 1 ; fi ; retries=$(($retries+1)) ; done
    - ./clair-scanner -c http://docker:6060 --ip $(hostname -i) -r gl-sast-container-report.json -l clair.log -w clair-whitelist.yml ${VERIFY_IMAGE} || true
  artifacts:
    paths: [gl-sast-container-report.json]

shellcheck:
  extends: .rules:code-changes
  stage: test
  needs: []
  image: koalaman/shellcheck-alpine:stable
  before_script:
    - apk add make
  script:
    - make shellcheck SHELL=/bin/sh

checkmake:
  stage: test
  needs: []
  extends: .rules:code-changes
  script:
    - PATH=${HOME}/go/bin:${PATH} make checkmake

yard:
  stage: test
  needs: []
  script:
    - bundle install --jobs 4 --path vendor/bundle
    - bundle exec yardoc
  cache:
    key: "ruby-2.7-bundle"
    paths:
      - ./vendor/bundle
  artifacts:
    expire_in: 1 week
    paths:
      - yard/*

verify-gdk-example-yml:
  extends: .rules:code-changes
  stage: test
  needs: []
  script:
    - make verify-gdk-example-yml

danger-review:
  extends:
    - .review:rules:danger
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:danger
  stage: test
  needs: []
  script:
    - danger --fail-on-errors=true --verbose
