# frozen_string_literal: true

module GDK
  GEM_VERSION = '0.2.12'
  VERSION = "GitLab Development Kit #{GEM_VERSION}"
end
