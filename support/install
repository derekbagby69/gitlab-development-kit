#!/usr/bin/env bash

set -euo pipefail

CURRENT_ASDF_DIR="${ASDF_DIR:-${HOME}/.asdf}"
ASDF_SH_PATH="${CURRENT_ASDF_DIR}/asdf.sh"

GDK_INSTALL_DIR="gitlab-development-kit"
REQUIRED_COMMANDS=(git make)

ensure_required_commands_exist() {
  for command in "${REQUIRED_COMMANDS[@]}"; do
    if ! command -v "${command}" > /dev/null 2>&1; then
      echo "ERROR: Please ensure ${command} is installed."
      exit 1
    fi
  done
}

clone_gdk_if_needed() {
  if [[ -d ${GDK_INSTALL_DIR} ]]; then
    echo "INFO: A ${GDK_INSTALL_DIR} directory already exists in the current working directory, resuming.."
  else
    git clone https://gitlab.com/gitlab-org/gitlab-development-kit.git ${GDK_INSTALL_DIR}
  fi
}

bootstrap() {
  make bootstrap
}

gdk_install() {
  # shellcheck disable=SC1090
  source "${ASDF_SH_PATH}"
  gdk install
}

ensure_required_commands_exist
clone_gdk_if_needed
cd ${GDK_INSTALL_DIR} || exit
bootstrap
gdk_install

echo
echo "INFO: To make sure GDK commands are available in this shell and ensure you're"
echo "INFO: in the newly installed GDK directory, please run:"
echo
echo "source \"${ASDF_SH_PATH}\""
echo "cd ${GDK_INSTALL_DIR}"
echo
